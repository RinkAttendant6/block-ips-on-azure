# Block IPs Automatically

Automatically block IP addresses and subnets (CIDRs) on Azure Active Directory.

## Prerequisites

- PowerShell Core

### Azure Active Directory

To block network objects on Azure Active Directory, you will need:

- An app registration with the following configuration:
    - API permissions:
        - Policy.Read.All
        - Policy.ReadWrite.ConditionalAccess
    - A certificate (client secret are not supported)
- A [named IP ranges location](https://docs.microsoft.com/en-us/azure/active-directory/conditional-access/location-condition)

This app will only add entries to the named location. Blocking must be configured using [Conditional Access](https://docs.microsoft.com/en-us/azure/active-directory/conditional-access/howto-conditional-access-policy-location).

## Usage

To be documented