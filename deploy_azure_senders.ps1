$Credentials = New-Object System.Management.Automation.PSCredential -ArgumentList ($Env:AAD_USERNAME, $(ConvertTo-SecureString -String $Env:AAD_PASSWORD -Force -AsPlainText))

$Credentials.UserName

,(Import-Csv -Path ".\data\bademails.txt" -Header email).email | & ".\src\scripts\Sync-AzureTenantSenderBlockList.ps1" -Id $Env:AAD_BLOCKLIST_NAME -Verbose -Credential $Credentials