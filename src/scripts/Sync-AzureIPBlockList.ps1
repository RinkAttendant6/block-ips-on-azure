#Requires -Modules Microsoft.Graph.Authentication,Microsoft.Graph.Identity.SignIns
<#
.SYNOPSIS
Updates an ipNamedLocation object in Azure Active Directory

.DESCRIPTION
This script is used for updating an IP blocklist in Azure Active Directory that can be used as part of a Conditional Access Policy.

.PARAMETER ClientId
Azure Active Directory app registration application ID

.PARAMETER TenantId
Azure Active Directory tenant ID

.PARAMETER Id
ID of the named location in Azure Active Directory

.PARAMETER Name
Name of the named location in Azure Active Directory. If multiple named locations exist with the same name, IPs will be set in the oldest one.

.PARAMETER CertificatePath
Path to certificate for authentication with this client ID

.PARAMETER PrivateKeyPassword
Password for the private key file. This parameter is not necessary if CertificatePath is a path in Certificate drive

.PARAMETER cidr
List of IP addresses and/or IP ranges to block. Can also be specified via pipeline input as a complete array object (non-enumerated)

#>
param(
    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistByName", HelpMessage = "Application ID")]
    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistById", HelpMessage = "Application ID")]
    [ValidateNotNullOrEmpty()]
    [String] $ClientId,

    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistByName", HelpMessage = "Tenant ID")]
    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistById", HelpMessage = "Tenant ID")]
    [ValidateNotNullOrEmpty()]
    [String] $TenantId,

    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistById", HelpMessage = "Named location ID")]
    [ValidateNotNullOrEmpty()]
    [String] $Id = "",

    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistByName", HelpMessage = "Named location name")]
    [ValidateNotNullOrEmpty()]
    [String] $Name = "",

    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistByName", HelpMessage = "Path to certificate from certificate store")]
    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistById", HelpMessage = "Path to certificate from certificate store")]
    [ValidateNotNullOrEmpty()]
    [String] $CertificatePath,

    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistByName", HelpMessage = "Password for private key")]
    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistById", HelpMessage = "Password for private key")]
    [SecureString] $PrivateKeyPassword,

    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistByName", HelpMessage = "IP address and/or range to block", ValueFromPipeline = $True)]
    [Parameter(Mandatory = $True, ParameterSetName = "BlocklistById", HelpMessage = "IP address and/or range to block", ValueFromPipeline = $True)]
    [ValidateNotNullOrEmpty()]
    [String[]] $cidr
)

$ErrorActionPreference = "Stop"

# Process the input

$regexes = @{
    "ip4"   = "^(?:(?:25[0-5]|2[0-4]\d|[01]?\d{1,2}?)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d{1,2})$"
    "cidr4" = "^(?:(?:25[0-5]|2[0-4]\d|[01]?\d{1,2}?)\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d{1,2})\/(?:[12]?\d|3[0-2])$"
    "ip6"   = "^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))$"
    "cidr6" = "^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))\/(?:\d{1,2}|1[0-1]\d|12[0-8])$"
}

[array]$maliciousIpRanges = $cidr | ForEach-Object {
    $line = $_.Trim()

    if ($line -imatch $regexes.cidr4 -or $line -imatch $regexes.cidr6) {
        @{cidrAddress = $line }
    }
    elseif ($line -imatch $regexes.ip4) {
        @{cidrAddress = "$line/32" }
    }
    elseif ($line -imatch $regexes.ip6) {
        @{cidrAddress = "$line/128" }
    }
    else {
        Write-Warning "$line is not a valid IP address or CIDR"
    }
}

if ($maliciousIpRanges.Count -eq 0) {
    Write-Host "Nothing to do"
    return
}

Write-Host ("{0} entries to block" -f $maliciousIpRanges.Count)

$payload = @{
    "@odata.type" = "#microsoft.graph.ipNamedLocation"
    isTrusted     = $false
    ipRanges      = $maliciousIpRanges
}

# Connect to Microsoft Graph API

$Cert = [System.Security.Cryptography.X509Certificates.X509Certificate2]::new($CertificatePath, $PrivateKeyPassword)

Connect-MgGraph -ClientID $ClientId -TenantId $TenantId -Certificate $Cert

try {
    # Find the named location
    if ($Id) {
        $blocklist = Get-MgIdentityConditionalAccessNamedLocation -NamedLocationId $id -Filter "isof('microsoft.graph.ipNamedLocation')"
    }
    else {
        $blocklist = Get-MgIdentityConditionalAccessNamedLocation -Filter "isof('microsoft.graph.ipNamedLocation') and DisplayName eq '$Name'" -Top 1 -Sort CreatedDateTime
    }

    if ($null -eq $blocklist) {
        throw "Invalid blocklist ID or name provided"
    }

    $blocklist | Format-Table -AutoSize

    # Do not add to trusted named locations
    if ($blocklist.AdditionalProperties["isTrusted"]) {
        throw ("{0} is a trusted ipNamedLocation object" -f $blocklist.Id)
    }

    # broken: see https://github.com/microsoftgraph/msgraph-sdk-powershell/issues/785
    # Update-MgIdentityConditionalAccessNamedLocation -NamedLocationId $blocklist.Id -BodyParameter $payload

    # temporary workaround:
    Invoke-MgGraphRequest -Method PATCH `
        -Uri ("https://graph.microsoft.com/v1.0/identity/conditionalAccess/namedLocations/{0}" -f $blocklist.Id) `
        -Body ($payload | ConvertTo-Json)
}
finally {
    Disconnect-MgGraph
}
