#Requires -Modules ExchangeOnlineManagement

[CmdletBinding()]

param (
    [Parameter(Mandatory = $true, HelpMessage = "Credentials")]
    [PSCredential]$Credential, 

    [Parameter(HelpMessage = "Identification value")]
    [String] $Id = "",

    [Parameter(HelpMessage = "Senders to block", ValueFromPipeline = $True)]
    [String[]] $Senders = @()
)

$ErrorActionPreference = "Stop"

# Process the input

$ValidSenders = $Senders | Where-Object {
    try {
        $null = [mailaddress]$_
        return $true
    }
    catch {
        Write-Warning $_
        return $false
    }
}

Connect-ExchangeOnline -Credential $Credential

try {
    $blocklist = (Get-TenantAllowBlockListItems -ListType Sender -Block | Where-Object { $_.Notes -eq $Id }).Value

    Write-Host ("{0} entries on existing blocklist" -f $blocklist.Count)

    $entriesToAdd = @($ValidSenders | Where-Object { $blocklist -notcontains $_ })
    $entriesToDelete = @($blocklist | Where-Object { $ValidSenders -notcontains $_ })

    Write-Host ("{0} entries to add" -f $entriesToAdd.Count)
    if ($entriesToAdd) {
        Write-Verbose ($entriesToAdd -join ", ")

        New-TenantAllowBlockListItems -ListType Sender -Block -Entries $entriesToAdd -NoExpiration -Notes $Id
    }

    Write-Host ("{0} entries to remove" -f $entriesToDelete.Count)
    if ($entriesToDelete) {
        Write-Verbose ($entriesToDelete -join ", ")

        Remove-TenantAllowBlockListItems -ListType Sender -Entries $entriesToDelete
    }

    Write-Host "Current blocklist"

    Get-TenantAllowBlockListItems -ListType Sender -Block | Where-Object { $_.Notes -eq $Id }
}
finally {
    Disconnect-ExchangeOnline -Confirm:$False
}
